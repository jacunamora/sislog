require "rails_helper"

RSpec.describe WsLogsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/ws_logs").to route_to("ws_logs#index")
    end

    it "routes to #new" do
      expect(:get => "/ws_logs/new").to route_to("ws_logs#new")
    end

    it "routes to #show" do
      expect(:get => "/ws_logs/1").to route_to("ws_logs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/ws_logs/1/edit").to route_to("ws_logs#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/ws_logs").to route_to("ws_logs#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/ws_logs/1").to route_to("ws_logs#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/ws_logs/1").to route_to("ws_logs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/ws_logs/1").to route_to("ws_logs#destroy", :id => "1")
    end

  end
end
