require 'rails_helper'
require 'sidekiq/testing'
require 'securerandom'
require 'json'
require 'sidekiq'


RSpec.describe WebServiceLogWorker, type: :model do
  context "Correctly enqueue a job" do
    before(:each) do
      # redis = Redis.new(:url => 'redis://127.0.0.1:6379/0', namespace: 'sys_log_test')
      #
      # redis.sadd("queues", "request_app")
      #
      # job = { "class" => 'WebServiceLogWorker',
      #         "queue" => 'request_app',
      #         "args" => [1, 2, 3],
      #         'retry' => true,
      #         'jid' => SecureRandom.hex(12),
      #         'created_at' => Time.now.to_f,
      #         'enqueued_at' => Time.now.to_f }
      # # redis.lpush("queue:request_app", JSON.dump(job))
      # byebug
      # puts "hola"

      Sidekiq::Client.enqueue_to(:request_app, WebServiceLogWorker, 'foo')

    end

    #byebug
    it { expect(WebServiceLogWorker.jobs.size).to eq(1)}
    it { expect(WsLog.count).to eq(1) }
  end

end