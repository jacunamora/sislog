require 'rails_helper'

RSpec.describe "ws_logs/show", type: :view do
  before(:each) do
    @wslog = assign(:wslog, Wslog.create!(
      :params => "Params",
      : => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Params/)
    expect(rendered).to match(/MyText/)
  end
end
