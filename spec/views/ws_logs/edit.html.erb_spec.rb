require 'rails_helper'

RSpec.describe "ws_logs/edit", type: :view do
  before(:each) do
    @wslog = assign(:wslog, Wslog.create!(
      :params => "MyString",
      : => "MyText"
    ))
  end

  it "renders the edit wslog form" do
    render

    assert_select "form[action=?][method=?]", wslog_path(@wslog), "post" do

      assert_select "input[name=?]", "wslog[params]"

      assert_select "textarea[name=?]", "wslog[]"
    end
  end
end
