require 'rails_helper'

RSpec.describe "ws_logs/index", type: :view do
  before(:each) do
    assign(:ws_logs, [
      Wslog.create!(
        :params => "Params",
        : => "MyText"
      ),
      Wslog.create!(
        :params => "Params",
        : => "MyText"
      )
    ])
  end

  it "renders a list of ws_logs" do
    render
    assert_select "tr>td", :text => "Params".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
