require 'rails_helper'

RSpec.describe "ws_logs/new", type: :view do
  before(:each) do
    assign(:wslog, Wslog.new(
      :params => "MyString",
      : => "MyText"
    ))
  end

  it "renders new wslog form" do
    render

    assert_select "form[action=?][method=?]", wslogs_path, "post" do

      assert_select "input[name=?]", "wslog[params]"

      assert_select "textarea[name=?]", "wslog[]"
    end
  end
end
