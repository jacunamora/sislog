require 'rails_helper'

RSpec.describe WsLog, type: :model do
  context "has no WsLog in the database" do
    it { expect(WsLog).to have(:no).records }
    it { expect(WsLog).to have(0).records   }
  end

  context "has one record" do
    before (:each) do
      params = {"code"=>0, "operation_tag"=>{"successful_processing"=>true, "system_name"=>"BM", "request_id"=>"1", "language_code"=>"ESPA", "external_user_code"=>"CMUNOZ", "operation_name"=>"ValidateAlias", "operation_id"=>"23072125efa0f4-28c8-419f-8304-1c61a549cd56", "time"=>"2018ene04230721", "gmt_time_zone"=>"-06:00:00", "result_code"=>"000000", "result_message"=>nil, "@xmlns"=>"http://sistemasgalileo.com/InternalServices/"}, "data"=>{"authentication_profile"=>"P01", "customer_id"=>"11013259", "delegate_id"=>"109830196", "status"=>"AC", "type_id"=>"WEBID", "login_authentication_type"=>{"authentication_type_bm"=>["KEYBOARD"]}, "allow_enter_account"=>false, "@xmlns"=>"http://sistemasgalileo.com/InternalServices/"}, "session_duration"=>666, "uuid"=>"d50c8cabd1e2730dd4c43e82382f4a25", "date"=>"04/01/2018 23:07:23"}
      w = WsLog.create(:params => params)
    end

    it { expect(WsLog).to have(1).record }
  end

  # it "counts only records that match a query" do
  #   WsLog.create!(:request => "Cog")
  #   expect(WsLog.where(:request => "Cog")).to have(1).record
  #   expect(WsLog.where(:request => "Wheel")).to have(0).records
  # end
  #
  #
  # it "has record with model function creater" do
  #   WsLog.create_ws_log(:fecha => "2017-12-21",
  #                         :request => "prueba",
  #                         :response => "prueba",
  #                         :origin => "prueba",
  #                         :destiny => "prueba",
  #                         :webkit => "prueba",
  #                         :version_so => "prueba",
  #                         :active => "false",
  #                         :service_identifier => "prueba",
  #                         :service_name => "prueba",
  #                         :category_code => "prueba",
  #                         :entity_code => "prueba",
  #                         :service_code => "prueba",
  #                         :bank_id => "prueba",
  #                         :category_name => "prueba",
  #                         :template_id => "prueba"
  #   )
  #   expect(WsLog).to have(1).record
  # end






end
