# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180109195317) do

  create_table "webservice_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.datetime "fecha"
    t.string "request"
    t.string "response"
    t.string "origin"
    t.string "destiny"
    t.string "webkit"
    t.string "version_so"
    t.boolean "active"
    t.string "service_identifier"
    t.string "service_name"
    t.string "category_code"
    t.string "entity_code"
    t.string "service_code"
    t.string "bank_id"
    t.string "category_name"
    t.string "template_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ws_logs", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.text "params"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
