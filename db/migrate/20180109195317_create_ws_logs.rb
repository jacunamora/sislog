class CreateWsLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :ws_logs do |t|
      t.text :params

      t.timestamps
    end
  end
end
