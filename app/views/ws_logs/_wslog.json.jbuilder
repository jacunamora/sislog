json.extract! wslog, :id, :params, :created_at, :updated_at
json.url wslog_url(wslog, format: :json)
