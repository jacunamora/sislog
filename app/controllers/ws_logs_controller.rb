class WsLogsController < ApplicationController
  before_action :set_wslog, only: [:show, :edit, :update, :destroy]

  # GET /ws_logs
  # GET /ws_logs.json
  def index
    @wslogs = WsLog.all
  end

  # GET /ws_logs/1
  # GET /ws_logs/1.json
  def show
  end

  # GET /ws_logs/new
  def new
    @wslog = WsLog.new
  end

  # GET /ws_logs/1/edit
  def edit
  end

  # POST /ws_logs
  # POST /ws_logs.json
  def create
    @wslog = WsLog.new(wslog_params)

    respond_to do |format|
      if @wslog.save
        format.html { redirect_to @wslog, notice: 'Wslog was successfully created.' }
        format.json { render :show, status: :created, location: @wslog }
      else
        format.html { render :new }
        format.json { render json: @wslog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ws_logs/1
  # PATCH/PUT /ws_logs/1.json
  def update
    respond_to do |format|
      if @wslog.update(wslog_params)
        format.html { redirect_to @wslog, notice: 'Wslog was successfully updated.' }
        format.json { render :show, status: :ok, location: @wslog }
      else
        format.html { render :edit }
        format.json { render json: @wslog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ws_logs/1
  # DELETE /ws_logs/1.json
  def destroy
    @wslog.destroy
    respond_to do |format|
      format.html { redirect_to wslogs_url, notice: 'Wslog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_wslog
      @wslog = WsLog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def wslog_params
      params.require(:wslog).permit(:params)
    end
end
