Sidekiq.configure_server do |config|
  config.redis = { :db => 0, :host => "127.0.0.1" }
end

Sidekiq.configure_client do |config|
  config.redis = { :db => 0,  :host => "127.0.0.1" }
end