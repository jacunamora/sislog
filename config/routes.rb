
Rails.application.routes.draw do
  resources :ws_logs
  require 'sidekiq/web'
  # require 'sidekiq/api'
  mount Sidekiq::Web => '/sidekiq'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  #mount Sidekiq::Web, at: '/sidekiq'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
