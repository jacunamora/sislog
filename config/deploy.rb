# config valid for current version and patch releases of Capistrano
lock "~> 3.10.1"

#set :application, "my_app_name"
#set :repo_url, "git@example.com:me/my_repo.git"

#set :repo_url, 'git@git-server.com:my-github-username/my-app.git'

#Activar cuando sea instalado ruby en rbenv
#set :rbenv_ruby, '2.4.1'

#server "10.3.1.157", :web, :app, :db, primary: true

#server '10.3.1.157', user: 'deployer', roles: %w{web app db}
#set :stage, :production

server "10.3.1.157", user: "deployer", roles: %w{app db web}, password: "123456"

#set :user, "deployer"
#set :password, "123456"
set :application, "sislog"
set :deploy_to, "/home/deployer/apps/sislog"
set :use_sudo, false
set :keep_releases, 5
set :deploy_via, :copy
#set :repository, "/Users/javier/Documents/proyectos/SisLog/"
set :repo_url, "https://jacunam@bitbucket.org/jacunam/sislog.git"
set :copy_exclude, %w[.git log tmp .DS_Store]

#set :scm, :none #deprecated

#default_run_options[:pty] = true

#set :stages, ["production"]

after "deploy", "deploy:cleanup" # keep only the last 10 releases

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure
